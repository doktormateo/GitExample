package qwerty.model;

public class Book {
    private String id;
    private String category;
    private String name;
    private double price;
    private boolean inStock;
    private String author;
    private String series;
    private String sequence;
    private String genre;
}
