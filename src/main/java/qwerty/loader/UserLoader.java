package qwerty.loader;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import qwerty.model.User;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UserLoader {
    public static List<User> getUserFromFile(File file) {
        List<User> users = new ArrayList<>();
        try {

            LineIterator lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String[] userRaw = lineIterator.nextLine().split(" ");
                User user = new User(userRaw[0], userRaw[1], Integer.valueOf(userRaw[2]));
                users.add(user);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return users;
    }

    public static List<User> getWomenFromFile(File file) {
        List<User> women = new ArrayList<>();
        for (User user : getUserFromFile(file)) {
            if (user.getName().endsWith("a")) {
                if (user.getAge() >= 18) {
                    women.add(user);
                }
            }
        }

        return women;
    }
        public  static List<User> getMenFromFile(File file){
            List <User> men = new ArrayList<>();
            for (User user: getUserFromFile(file) ){
                if(!user.getName().endsWith("a")) {
                    if (user.getAge() >= 18) {
                        men.add(user);
                    }
                }
            }return men;
        }

}

